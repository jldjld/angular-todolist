import { Component } from '@angular/core';
import { TodoItem } from './entity/todoItem/todoItem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'todolist-client';
  todoList: TodoItem[] = [];
}

import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpClient, HttpResponse, HttpStatusCode } from '@angular/common/http';
import { BehaviorSubject, Observable, catchError, filter, map, switchMap, take, tap, throwError } from 'rxjs';
import { AuthService } from './services/auth/auth.service';
import { environment } from './environments/environment.dev';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
  //private isRefreshing = false;
  //private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
        const token    = this.authService.state;
        const isApiUrl = request.url.startsWith(environment.todosURL);
        if( typeof token === "string" && token !== null && isApiUrl) {
            request = this.addTokenHeader(request, token);

            return next.handle(request)
        }
        
        return next.handle(request).pipe(
          tap({
            error: (error: any) => {
              // WIP REFRESH TOKEN IMPLEMENTATION //
              /*if (error instanceof HttpErrorResponse &&
                request.url.includes(environment.todosURL) &&
                error.status === HttpStatusCode.Forbidden) {
                return this.handle403Error(request, next);
              }*/

              return throwError(() => error);
            }
          }
        )
      )
    }

    /*private handle403Error(request: HttpRequest<any>, next: HttpHandler) {
      if (!this.isRefreshing) {
        this.isRefreshing = true;
        this.refreshTokenSubject.next(null);
        const token = this.authService.state;

        if ( typeof token === "string" && token.length !== 0 ) {
          return this.authService.refreshToken(token).pipe(
              map((token: any) => {
                this.isRefreshing = false;
    console.log(token)
                this.authService.saveToken(token.accessToken);
                this.refreshTokenSubject.next(token.accessToken);
                
                return next.handle(this.addTokenHeader(request, token))
            }),
            catchError((error) => {
              this.isRefreshing = false;
              console.log(token);
              return throwError(() => error);
            })
          );
        }
      }
  
      return this.refreshTokenSubject.pipe(
            filter(token => token !== null),
            take(1),
            switchMap((token) => next.handle(this.addTokenHeader(request, token))
        ));
    }
    */
    private addTokenHeader(request: HttpRequest<any>, token: string) {
      return request.clone({ headers: request.headers.set('Authorization', `Bearer ${token}`) });
    }
}

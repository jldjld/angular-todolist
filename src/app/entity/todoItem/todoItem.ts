export interface TodoItem {
    id:number;
    content:string;
    completed:boolean;
}

const DEFAULT_TODOITEM: TodoItem = {
    id: 0,
    content: "",
    completed: false
};
import { Component, Input, OnInit } from '@angular/core';
import { TodoItem } from '../entity/todoItem/todoItem';
import { TodoListService } from '../services/todoList/todo-list.service';
import { AuthService } from '../services/auth/auth.service';
import { HttpStatusCode } from '@angular/common/http';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  @Input() todoList: TodoItem[] = [];
  authState = this.authService.state;
  hasError : boolean = false;

  constructor(private todoListService : TodoListService, private authService : AuthService) {}

  ngOnInit() {
    // Get token on component init
    this.getToken();

    // Get todoList content if already exists
    this.getResults();

    // Start request if todolist content is empty
    if ( this.todoList.length === 0 ) {
      this.getTodoList();
    }
  }

  getToken() {
    let token = this.authService.state;
    if( token === null ) {
      this.authService.setToken().subscribe({
        next: response => {
          if (response.principal !== null) {
            return response.principal;
          }
          
          return response;
        },
        error: error => {
          throw new Error(error);
        }
      });
    }

    return token;
  }

  getResults() {
    const results = this.todoListService.getResults();
    if( results.length !== 0 ) {
      this.todoList = results;
    }

    return this.todoList;
  }

  getTodoList() {
    return this.todoListService.findAll().subscribe({
      next: todos => this.todoList = todos,
      error: (error) => {
        this.hasError = true;
        if(error.status === HttpStatusCode.BadRequest) {
          throw new Error("Issue on server side, while getting todolist.");
        }
      }
    });
  }

  addTodo(newTodo: string) {
    if( newTodo.length === 0 ){
      alert("Please enter a value.");
      return document.getElementById('newTodo')?.focus();
    }
    
    return this.todoListService.addTodo( newTodo ).subscribe({
        next: todo => this.todoList.push(todo),
        error: (error) => { 
          this.hasError = true;
          if(error.status === HttpStatusCode.Conflict) {
            alert("Task already exists in your todolist.");
          }

          if(error.status === HttpStatusCode.UnprocessableEntity) {
            throw new Error("Issue on server side while creating a new task for todo : " + newTodo);
          }
        }
      }
    );
  }

  completeTodo(todoId : number) {
    if( todoId === null ) {
      throw new Error("Todo ID is missing.");
    }

    return this.todoListService.completeTodo(todoId).subscribe({
      next: response => {
        // Updating todo completed status
        const todoIndex                    = this.todoList.findIndex((obj => obj.id == todoId));
        this.todoList[todoIndex].completed = true;

        if(response === HttpStatusCode.NoContent) {
          alert("Task is completed !");
        } 
      },
      error: (error) => { 
        this.hasError = true;
        if(error.status === HttpStatusCode.NotFound) {
          throw new Error("Issue on server side while getting todo with id : " + todoId);
        }

        if(error.status === HttpStatusCode.UnprocessableEntity) {
          throw new Error("Issue on server side while saving todo with id : " + todoId);
        }
      }
    });
  }

  /*searchTodoItem( searchedContent: String ) : TodoItem {

    this.results = this.todoList.filter(todo => 
      todo.content.toLowerCase().includes(searchedContent.toLowerCase()
      )
    )
  }*/
}

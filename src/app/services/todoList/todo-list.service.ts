import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TodoItem } from '../../entity/todoItem/todoItem';
import { Observable, catchError, map, throwError } from 'rxjs';
import { environment } from '../../environments/environment.dev';

@Injectable({
  providedIn: 'root'
})

export class TodoListService {
  private todoList: TodoItem[] = [];

  constructor(private http:HttpClient) {
  }

  findAll() : Observable<TodoItem[]> { 
    return this.http.get<TodoItem[]>(environment.todosURL)
    .pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(() => error);
      }),
      map(
        (todos: TodoItem[]) => {
          if(todos === null ) {
            throw new Error('Todolist is empty.');
          }

          todos.forEach(todo => {
            if( !this.todoList.includes(todo) ) {
              this.todoList.push(todo);
            }
          })

          return todos;
        }
      )
    );
  }

  addTodo( newTodo: string ) : Observable<TodoItem> {
    return this.http.post<TodoItem>(environment.addTodoURL, { content : newTodo }).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(() => error);
      }),
      map(
        (todo : TodoItem) => {
          if( todo !== null && !this.todoList.includes(todo) ) {
            this.todoList.push(todo);
          }

          return todo;
        }
      )
    );
  }

  completeTodo(id: number) : Observable<Object> {
    // Observe response needed to get httpcodestatus data
    return this.http.post(environment.todosURL + "/" + id + "/complete", {}, {observe: 'response'}).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(() => error);
      }),
      map(
        response => {
          return response.status;
        }
      )
    )
  }

  getResults() : TodoItem[] {
    return this.todoList;
  }
}

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TodoListService } from './todo-list.service';
import { TodoItem } from '../../entity/todoItem/todoItem';
import { environment } from '../../environments/environment.dev';
import { HttpResponse, HttpStatusCode } from '@angular/common/http';

describe('TodoListService', () => {
    let service: TodoListService;
    let httpTestingController: HttpTestingController;
    let todoList: TodoItem[];
    let httpResponse: HttpResponse<TodoItem>;
    let firstTodo : TodoItem;
    let secondTodo : TodoItem;
    let mockedTodolist: TodoItem[];

    const testCases = [
      3, "1", {0: 0}, [1], ""
    ];

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ HttpClientTestingModule ],
        providers: [ TodoListService ]
      });

      httpTestingController = TestBed.inject(HttpTestingController);
      service = TestBed.inject(TodoListService);

      firstTodo = {
        id : 0,
        content: "sel",
        completed: false
      };

      secondTodo = {
        id : 1,
        content: "poivre",
        completed: false
      };
      
      mockedTodolist = [
        firstTodo,
        secondTodo
      ];

      service.addTodo("sel");
      service.addTodo("poivre");

      todoList = service.getResults();
    });

    afterEach(() => {
      httpTestingController.verify();
    });

    // GETTODOLIST OK
    it('Should getTodolist()', () => {
      service.findAll().subscribe((data) => {
        expect(data).toEqual(todoList);
      });

      const req = httpTestingController.expectOne(environment.todosURL);
      expect(req.request.method).toEqual('GET');
      req.flush(mockedTodolist);
    });

    // COMPLETETODO OK
    it('Should completeTodo()', () => {
      service.completeTodo( 1 ).subscribe({
        error: (response) => {
          expect(response.status).toEqual(HttpStatusCode.NoContent);
        }
      });

      const expectedCompleteResponse = new HttpResponse({ status: 204, statusText: "No content" });
      const completeReq = httpTestingController.expectOne(environment.todosURL + "/" + 1 + "/complete");
      completeReq.flush(expectedCompleteResponse);
    });

    // COMPLETETODO KO
    testCases.forEach((value) => {
        it('Should not completeTodo() for value : ' + value, () => {
        service.completeTodo(value as any).subscribe({
            error: (error) => {
              expect(error.status).toEqual(HttpStatusCode.NotFound);
            }
        });

        const notCompleteReq = httpTestingController.expectOne(environment.todosURL + "/" + value + "/complete");
        expect(notCompleteReq.request.method).toEqual('POST');
        notCompleteReq.flush(HttpStatusCode.NotFound);
      });
    })

    // ADDTODO OK
    it('Should addTodo() and return 200 status', () => {
      let newTodoItem : TodoItem = {
        id: 8,
        content: "thyme",
        completed: false
      }

      const expectedResponse = new HttpResponse({ status: 200, statusText: "Success", body: newTodoItem });
      service.addTodo("thyme").subscribe(newTodo => {
        httpResponse = expectedResponse;
        expect(newTodo).toEqual(newTodoItem);
      });

      const addTodoReq = httpTestingController.expectOne(environment.addTodoURL);
      addTodoReq.flush(newTodoItem, expectedResponse);

      expect(addTodoReq.request.method).toEqual('POST');
      expect(httpResponse).toEqual(expectedResponse);
      // Verify if newTodo is added to todoList
      expect(service.getResults()).toContain(newTodoItem);
    });

    // ADDTODO KO
    testCases.forEach((value) => {
    it('Should not addTodo() if content type is not correct', () => {
        service.addTodo(value as any).subscribe({
            error: (error) => {
            expect(error.status).toEqual(HttpStatusCode.NotFound);
          }
        });

        let koTodoItem : TodoItem = {
          id: 8,
          content: value as any,
          completed: false
        }

        const addTodoReq = httpTestingController.expectOne(environment.addTodoURL);
        expect(addTodoReq.request.method).toEqual('POST');
        // Item should not exists in todoList
        expect(service.getResults()).not.toContain(koTodoItem);
        addTodoReq.flush(HttpStatusCode.NotFound);
      });
    })

    it('Should not addTodo() if content already exists', () => {
      service.addTodo("sel").subscribe({
          error: (error) => {
          expect(error.status).toEqual(HttpStatusCode.Conflict);
        }
      })

      const addTodoReq = httpTestingController.expectOne(environment.addTodoURL);
      expect(addTodoReq.request.method).toEqual('POST');
      addTodoReq.flush(HttpStatusCode.Conflict);
    });
})

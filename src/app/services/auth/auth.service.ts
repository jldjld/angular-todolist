import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.dev';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  state: Object|null = {};

  constructor(private http:HttpClient) {
    const connected = localStorage.getItem('todo_token');
    if(connected?.length !== 0) {
      this.state = connected;
    }
  }
  
  setToken(): Observable<any> {
    return this.http.get<{ principal: string, expiration: string }>(environment.getTokenUrl, {
      headers: new HttpHeaders({ 'X-Shared-Secret': environment.secretKey })
    }).pipe(
      map((response) => {
        this.state = response;
        if (response.principal !== null || response.expiration !== null ) {
          
          return this.saveToken(response.principal);;
        }
    }),
    catchError((error: HttpErrorResponse) => {
      return throwError(() => error);
    })
    );
  }

  saveToken(token: string) {
    localStorage.removeItem('todo_token');
    localStorage.setItem("todo_token", token);
  }
/*
  refreshToken(token: string) {
    return this.http.post(environment.refreshTokenUrl,
      { refresh: token }, httpOptions).pipe(
        map((response) => {
          console.log(response);
        }
        )
      );
  }*/
}

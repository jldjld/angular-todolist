export const environmentExample = {
    production: false,
    title: 'Local Environment TodoList',
    apiURL: 'xxxxxxx',
    addTodoURL: 'xxxxxxx',
    getTokenUrl: 'xxxxxx',
    secretKey: 'xxxxxx'
  };